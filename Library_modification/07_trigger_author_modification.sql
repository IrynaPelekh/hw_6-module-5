use ip_library
go

DROP TRIGGER IF EXISTS tr_author_modif
go

CREATE trigger tr_author_modif ON book
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
   UPDATE author
   SET author.book_amount=COALESCE((SELECT COUNT(*) 
   FROM book as b INNER JOIN author_book as a_b
   ON b.ISBN=a_b.ISBN and a_b.author_id=author.author_id),0)
   WHERE author.author_id IN (SELECT * FROM(SELECT a_b.author_id 
   FROM inserted as i INNER JOIN author_book as a_b
   ON i.ISBN=a_b.ISBN) as INS UNION ALL
   SELECT * FROM(SELECT a_b.author_id 
   FROM deleted as d INNER JOIN author_book as a_b
   ON d.ISBN=a_b.ISBN) as DEL);

   UPDATE author
   SET author.issue_amount=COALESCE((SELECT COUNT(*) 
   FROM book as b INNER JOIN author_book as a_b
   ON b.ISBN=a_b.ISBN and a_b.author_id=author.author_id),0)
   WHERE author.author_id IN (SELECT * FROM(SELECT a_b.author_id 
   FROM inserted as i inner join author_book as a_b
   ON i.ISBN=a_b.ISBN) as INS UNION ALL
   SELECT * FROM(SELECT a_b.author_id 
   FROM deleted as d inner join author_book as a_b
   ON d.ISBN=a_b.ISBN) as DEL);

   UPDATE author
   SET author.total_edition=COALESCE((SELECT SUM(b.edition) 
   FROM book as b INNER JOIN author_book as a_b
   ON b.ISBN=a_b.ISBN and a_b.author_id=author.author_id),0)
   WHERE author.author_id IN (SELECT * FROM(SELECT a_b.author_id 
   FROM inserted as i INNER JOIN author_book as a_b
   ON i.ISBN=a_b.ISBN) as INS UNION ALL
   SELECT * FROM(SELECT a_b.author_id 
   FROM deleted as d INNER JOIN author_book as a_b
   ON d.ISBN=a_b.ISBN) as DEL);
END
