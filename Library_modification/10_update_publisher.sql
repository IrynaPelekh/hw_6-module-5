use ip_library
go

update publisher
set
    book_amount = 4500, issue_amount = 8000, total_edition = 75000
	where publisher_id = 4
go

update publisher
set book_amount = 5000, issue_amount = 8500, total_edition = 85000
where publisher_id = 3
go

update publisher
set book_amount = 6000, issue_amount = 9500, total_edition = 95000
where publisher_id = 2
go

update publisher
set book_amount = 2500, issue_amount = 4000, total_edition = 22000
where publisher_id = 1
go

update publisher
set book_amount = 5500, issue_amount = 9200, total_edition =
10000
where publisher_id = 5
go


select * from publisher
go
