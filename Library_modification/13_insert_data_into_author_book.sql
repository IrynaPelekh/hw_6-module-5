use ip_library
go

insert into author_book
(author_book_id, ISBN, author_id, updated, updated_by) values 
(10,'978-6-175-85100-5', 2, NULL, NULL),
(11, '978-1-484-23444-0',11, NULL, NULL),
(12, '978-1-484-23444-0',12, NULL, NULL),
(13, '978-1-484-23444-0',13, NULL, NULL),
(14, '978-1-484-21964-5',14, NULL, NULL),
(15, '978-0-349-14131-2',15, NULL, NULL),
(16, '978-1-786-49423-8',16, NULL, NULL)
go
