use ip_library
go

ALTER TABLE publisher
DROP COLUMN IF EXISTS created
go

ALTER TABLE publisher
DROP COLUMN IF EXISTS country
go

ALTER TABLE publisher
DROP COLUMN IF EXISTS city
go

ALTER TABLE publisher
DROP COLUMN IF EXISTS book_amount
go

ALTER TABLE publisher
DROP COLUMN IF EXISTS issue_amount
go

ALTER TABLE publisher
DROP COLUMN IF EXISTS total_edition
go

ALTER TABLE publisher
ADD created date DEFAULT('20180101') NOT NULL,
country varchar(50) NOT NULL DEFAULT('USA'),
city varchar(50) NOT NULL DEFAULT('Washington'),
book_amount int NOT NULL DEFAULT(0) CHECK(book_amount>=0),
issue_amount int NOT NULL DEFAULT(0) CHECK(issue_amount>=0),
total_edition int NOT NULL DEFAULT(0) CHECK(total_edition>=0)
go