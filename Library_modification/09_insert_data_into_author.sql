use ip_library
go

insert into author
(author_id, name, URL,  updated, updated_by, book_amount, issue_amount, total_edition) values 
(NEXT VALUE FOR seq1_ip_library,'Bikramaditya_Singhal', 'www.b_singhal.com', NULL, NULL, 5, 8, 2000),
(NEXT VALUE FOR seq1_ip_library,'Gautam_Dhameja', 'www.g_dhameja.com', NULL, NULL, 6, 10, 5000),
(NEXT VALUE FOR seq1_ip_library,'Priyansu_Panda', 'www.p_panda.com', NULL, NULL, 3, 5, 700),
(NEXT VALUE FOR seq1_ip_library,'Dmitri_Korotkevitch', 'www.d_korotkevitch.com', NULL, NULL, 3, 5, 800),
(NEXT VALUE FOR seq1_ip_library,'Robin_Wasserman', 'www.r_wasserman.com', NULL, NULL, 6, 8, 4000),
(NEXT VALUE FOR seq1_ip_library,'William_Hartston', 'www.w_hartston.com', NULL, NULL, 3, 4, 500)

go

select * from author
go




