use triger_fk_cursor
go

drop trigger if exists tr__not_post_modification
go


create trigger tr__not_post_modification on post
after delete
as
	begin
		print 'Error. Deletion is forbidden'
		rollback tran
	end
go
