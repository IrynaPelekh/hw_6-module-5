use [triger_fk_cursor]
go

declare @id int
declare @surname varchar(30)
declare @name char(30)
declare @post varchar(15)
declare @create varchar(max)
declare @column int
declare @i int
declare @table varchar(120)
declare @type varchar(150)

declare employee_using_cursor cursor for 
	select id, surname, name, post from employee

open employee_using_cursor

fetch next from employee_using_cursor into @id, @surname, @name, @post

while @@FETCH_STATUS = 0
     begin
        set @table=@surname+rtrim(@name)+rtrim(@post)+cast(@id as varchar(3))
        set @create='Drop table if exist '+@table +' create table'
        set @i=1
        set @column=ABS(CHECKSUM(NEWID()))%9+1
        set @create = @create+@table+'('
        while @i<=@column
            begin
                if @i%2=0 set @type='int'
                else set @type='char(10)'
                set @create=@create+'column'+cast(@i as varchar(3))+' '+@type+','
                set @i+=1
            end
        set @create=left(@create,len(@create)-1)+')'
        exec(@create)
        fetch next from employee_using_cursor into @id, @surname, @name, @post
     end

close employee_using_cursor
deallocate employee_using_cursor
go



