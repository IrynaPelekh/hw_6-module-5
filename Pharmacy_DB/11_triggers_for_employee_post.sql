use triger_fk_cursor
go

drop trigger if exists tr_key_employee_post_del
go

create trigger tr_key_employee_post_del on post
after delete
as
  begin
	 delete from employee
	 from deleted 
	 inner join employee on deleted.post=employee.post
  end
go

----------

drop trigger if exists tr_key_employee_post_ins_upd
go

create trigger tr_key_employee_post_ins_upd on employee
after insert, update
as
  begin
   if exists ((select post from inserted where post is not null) 
   except (select post from post))
	 begin
		print 'fk creating error on employee post'
		rollback tran
	 end
  end
go
