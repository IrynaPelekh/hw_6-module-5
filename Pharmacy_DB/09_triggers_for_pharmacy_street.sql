use triger_fk_cursor
go

drop trigger if exists tr_key_pharmacy_street_del_upd
go

create trigger tr_key_pharmacy_street_del_upd on street
after update, delete
as
  begin
	update pharmacy
	set pharmacy.street=null
	from deleted 
       inner join pharmacy on deleted.street=pharmacy.street
  end
go

------------

drop trigger if exists tr_key_pharmacy_street_del_ins
go

create trigger tr_key_pharmacy_street_del_ins on pharmacy
after insert, update
as
  begin
	if exists ((select street from inserted where street is not null)
       except (select street from street))
		begin
		    print 'fk creating error on pharmacy street'
		    rollback tran
		end
  end
go