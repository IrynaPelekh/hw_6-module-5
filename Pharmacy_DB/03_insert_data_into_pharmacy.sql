use triger_fk_cursor
go

INSERT INTO pharmacy(name, building_number, www, work_time, saturday, sunday, street)
VALUES ('3i', 1, 'www.3i.com', '19:00', 1, 1, 'Street1'),
       ('Bam', 2, 'www.bam.com', '20:00', 1, 1, 'Street2'),
       ('LPA', 3, 'www.lpa.com', '21:00', 1, 0, 'Street3'),
       ('DS', 4, 'www.ds.com', '22:00', 1, 1, 'Street4'),
       ('Pharmacy24', 5, 'www.pharmacy24.com', '20:00', 1, 0, 'Street5')
go

select * from pharmacy
go