use triger_fk_cursor
go

drop trigger if exists tr_key_pharmacy_employee_del
go

create trigger tr_key_pharmacy_employee_del on pharmacy
after delete
as
  begin
	update employee
	set employee.pharmacy_id=null
	from deleted 
     inner join employee on deleted.id=employee.pharmacy_id
  end
go

----------

drop trigger if exists tr_key_pharmacy_employee_ins_upd
go

create trigger tr_key_pharmacy_employee_ins_upd on employee
after insert, update
as
  begin
	if exists ((select pharmacy_id from inserted 
where pharmacy_id is not null) except (select id from pharmacy))
	  begin
		print 'fk creating error on pharmacy employee'
		rollback tran
	  end
  end
go
