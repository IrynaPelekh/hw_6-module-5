use triger_fk_cursor
go

drop trigger if exists tr_key_zone_medicine_zone_del_casc
go

create trigger tr_key_zone_medicine_zone_del_casc on zone
after delete
as
  begin
	delete from medicine_zone
	from deleted 
     inner join medicine_zone 
     on deleted.id=medicine_zone.zone_id
  end
go

---------

drop trigger if exists tr_key_zone_medicine_zone_ins_upd_casc
go

create trigger tr_key_zone_medicine_zone_ins_upd_casc on medicine_zone
after insert, update
as
  begin
	if exists ((select zone_id from inserted 
      where zone_id is not null) 
      except (select id from zone))
	    begin
		   print 'fk creating error on zone medicine zone'
	        rollback tran
	    end
  end
go

-----------

drop trigger if exists tr_key_medicine_medicine_zone_del
go

create trigger tr_key_medicine_medicine_zone_del on medicine
after delete
as
  begin
	if exists ((select id from deleted) 
     intersect (select medicine_id from medicine_zone))
		begin
		    print 'fk creating error on medicine medicine_zone'
		    rollback tran
		end
  end
go

----------

drop trigger if exists tr_key_medicine_medicine_zone_ins_upd
go

create trigger tr_key_medicine_medicine_zone_ins_upd on medicine_zone
after insert, update
as
  begin
	if exists ((select medicine_id from inserted) 
     except (select id from medicine))
		begin
		  print 'fk creating error on medicine medicine_zone'
		  rollback tran
		end
  end
go