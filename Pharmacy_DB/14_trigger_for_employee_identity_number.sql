use triger_fk_cursor
go

drop trigger if exists tr_employee_id_number
go

create trigger tr_employee_id_number on employee
after insert, update
as
  begin
	if exists(select * from inserted 
     where right(identity_number,2)='00')
		begin
			print 'Error. Identity number can not 00 end'
			rollback tran
		end
  end
go
