use triger_fk_cursor
go

drop trigger if exists tr_key_medicine_pharmacy_medicine_del
go

create trigger tr_key_medicine_pharmacy_medicine_del on medicine
after delete
as
  begin
	if exists ((select id from deleted) 
     intersect (select medicine_id from pharmacy_medicine))
		begin
		   print 'fk creating error on medicine_pharmacy_medicine'
		   rollback tran
		end
  end
go

------------

drop trigger if exists tr_key_medicine_pharmacy_medicine_ins_upd
go

create trigger tr_key_medicine_pharmacy_medicine_ins_upd on pharmacy_medicine
after insert, update
as
	begin
		if exists ((select medicine_id from inserted) except (select id from medicine))
			begin
				print 'fk creating error on medicine_pharmacy_medicine'
				rollback tran
			end
	end
	go

----------

drop trigger if exists tr_key_pharmacy_pharmacy_medicine_del
go

create trigger tr_key_pharmacy_pharmacy_medicine_del on pharmacy
after delete
as
	begin
		if exists ((select id from deleted) intersect (select pharmacy_id from pharmacy_medicine))
			begin
				print 'fk creating error on pharmacy_pharmacy_medicine'
				rollback tran
			end
	end
go

---------

drop trigger if exists tr_key_pharmacy_pharmacy_medicine_ins_upd
go

create trigger tr_key_pharmacy_pharmacy_medicine_ins_upd on pharmacy_medicine
after insert, update
as
	begin
		if exists ((select pharmacy_id from inserted) except (select id from pharmacy))
			begin
				print 'fk creating error on pharmacy_pharmacy_medicine'
				rollback tran
			end
	end
go