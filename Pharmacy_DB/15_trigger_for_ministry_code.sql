use triger_fk_cursor
go

drop trigger if exists tr_ministry_code
go

create trigger tr_ministry_code on medicine
after insert, update
as
  begin
	if exists(select * from inserted where ministry_code not like '[a-z][a-z]-[0-9][0-9][0-9]-[0-9][0-9]' or ministry_code like '[mp][mp]%')
		begin
			print 'Error. Not correct ministry_code'
			rollback tran
		end
  end
go