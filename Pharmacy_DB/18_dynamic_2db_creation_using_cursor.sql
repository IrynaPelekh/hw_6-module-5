use triger_fk_cursor
go

declare @id INT
declare @surname VARCHAR(30)
declare @name CHAR(30)
declare @midle_name VARCHAR(30)
declare	@identity_number CHAR(10)
declare @passport CHAR(10)
declare @experience DECIMAL(10, 1)
declare @birthday DATE
declare @post VARCHAR(15)
declare @pharmacy_id INT

declare @table1 varchar(50)
declare @table2 varchar(50)
declare @create1 varchar(max)
declare @create2 varchar(max)
declare @table_description varchar(max)
declare @row varchar(max)
declare @row_description varchar(max)

declare random_tables_creating_cursor cursor
for
	SELECT id,surname,name,midle_name,identity_number,
		   passport,experience,birthday,post,pharmacy_id
	FROM employee

open random_tables_creating_cursor
fetch next from random_tables_creating_cursor 
     into @id,@surname, @name, @midle_name, @identity_number,
          @passport, @experience, @birthday, @post, @pharmacy_id

set @table_description='(id INT NOT NULL, surname VARCHAR(30) NOT NULL, name CHAR(30) NOT NULL, midle_name VARCHAR(30),
    identity_number CHAR(10), passport CHAR(10), experience DECIMAL(10, 1), birthday DATE,
    post VARCHAR(15) NOT NULL, pharmacy_id INT)'

set @row_description='(id,surname,name,midle_name,identity_number,passport,experience,birthday,post,pharmacy_id)'

set @table1='[table_'+cast(cast(current_timestamp as time(0)) as char(8))+']'
set @table2='[table_'+cast(cast(current_timestamp as time(0)) as char(8))+']'

set @create1='create table '+ @table1 + @table_description +' '+
				'create table ' + @table2 + @table_description+';'

exec(@create1)


while @@FETCH_STATUS=0
	begin
		set @row=RTRIM(cast(@id as varchar(5)))+','+''''+RTRIM(@surname)+''''+','+''''+RTRIM(@name)+''''+','
			+coalesce(quotename(RTRIM(@midle_name),''''),'null')+','+coalesce(quotename(RTRIM(@identity_number),''''),'null')+','
			+coalesce(quotename(RTRIM(@passport),''''),'null')+','+coalesce(RTRIM(cast(@experience as varchar(5))),'null')+','+
			coalesce(quotename(RTRIM(cast(@birthday AS VARCHAR(10))),''''),'null')+','
			+''''+RTRIM(@post)+''''+','+coalesce(RTRIM(cast(@pharmacy_id as varchar(5))),'null')

		if rand()>0.5
			begin
				set @create2='insert into '+@table1+@row_description+' VALUES('+@row+')'
			end
		else
			begin
				set @create2='insert into '+@table2+@row_description+' VALUES('+@row+')'
			end

		exec(@create2)

		fetch next from random_tables_creating_cursor 
                     into @id, @surname, @name, @midle_name, @identity_number,
                          @passport, @experience, @birthday, @post, @pharmacy_id
	end

close random_tables_creating_cursor
deallocate random_tables_creating_cursor
go