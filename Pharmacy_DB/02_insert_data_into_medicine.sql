use triger_fk_cursor
go

INSERT INTO medicine (name, ministry_code, recipe, narcotic, psychotropic)
VALUES ('Aspirin', '0000001', 0, 0, 0),
       ('Analgin', '0000002', 0, 0, 0),
       ('Spasmalgon', '0000003', 0, 0, 0),
       ('Sorbex', '0000004', 0, 0, 0),
       ('Sanax', '0000005', 1, 1, 1),
       ('Nimesil', '0000006', 0, 0, 0)
go

select * from medicine
go